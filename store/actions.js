
import axios from '../axios';


export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";

export const fetchContactsSuccess = (data) => {
    return {type: FETCH_CONTACTS_SUCCESS, data};
};


export const fetchContacts = () => {
    return (dispatch) => {
        axios.get('./contacts.json')
            .then(response => {
                console.log(response.data);
                dispatch(fetchContactsSuccess(response.data));
            })
    }
};
