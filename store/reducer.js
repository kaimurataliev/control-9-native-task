import * as actions from './actions';

const initialState = {
    contacts: {},
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.data};

        default:
            return state;
    }
};

export default reducer;