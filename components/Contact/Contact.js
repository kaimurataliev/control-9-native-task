import React from 'react';
import { Text, Image, TouchableOpacity } from 'react-native';
import ContactStyle from './ContactStyle';


const Contact = props => {

    const contacts = Object.keys(props.contacts);

    return (
        contacts.map((contact, index) => {
            return (
                <TouchableOpacity style={ContactStyle.contact} key={index}>
                    <Image style={ContactStyle.photo} source={{uri : props.contacts[contact].photo}}/>
                    <Text style={ContactStyle.text}>Name: {props.contacts[contact].name}</Text>
                </TouchableOpacity>
            )
        })
    )
};

export default Contact;