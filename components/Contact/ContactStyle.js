
const ContactStyle = {
    contact: {
        padding: 15,
        backgroundColor: '#bbbbbb',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 20
    },
    photo: {
        width: 60,
        height: 60,
    }
};

export default ContactStyle;