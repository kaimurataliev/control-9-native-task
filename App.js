import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';


import MainContainer from './containers/MainContainer/MainContainer';
import reducer from './store/reducer';

const store = createStore(reducer, applyMiddleware(thunk));

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <MainContainer />
        </Provider>
    );
  }
}
