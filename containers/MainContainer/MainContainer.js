import React, { Component } from 'react';
import { connect } from 'react-redux';
import {ScrollView, Modal, TouchableOpacity, Text, View, Button} from 'react-native';
import {fetchContacts} from "../../store/actions";
import Contact from "../../components/Contact/Contact";
import MainStyle from './MainStyle';

class MainContainer extends  Component {

    state = {
        showModal: false
    };

    componentDidMount() {
        this.props.fetchContacts();
    }

    toggleModal = () => {
        this.setState(prevState => ({
                showModal: !prevState.showModal
            }
        ))
    };

    render() {

        const contacts = Object.keys(this.props.contacts)

        return (
            <ScrollView style={MainStyle.container}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.showModal}
                    onRequestClose={this.toggleModal}
                >

                </Modal>
                <TouchableOpacity onPress={this.toggleModal} title='show'>
                    <Text>Toggle modal</Text>
                </TouchableOpacity>
                <Contact contacts={this.props.contacts}/>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchContacts: () => dispatch(fetchContacts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);